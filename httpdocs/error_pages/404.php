<?php
include_once(SRV_ROOT."admin/pages/class.page.php");
include_once(SRV_ROOT."includes/functions.php");

$level1 = "404";
$this_slug = $level1;
$parent_slug = '';

$tr_page = new page('',$this_slug,$parent_slug,SITE_LANG);

if($tr_page->active == 1 || ( $user->loggedin && $user->user_groupID < 3))
{	
	$tr_page_path = $tr_page->get_path();
	
	$path='';
	foreach($tr_page_path as $node)
	{
		$path .= $node['slug']."/";
	}
	
	//Redirect to first child?
	if($tr_page->leaf == false && $tr_page->parentAsPage == '0')
	{
		$child = $tr_page->get_first_child();
		if(!is_null($child))
		{
			$tr_child = new page($child);
			header("location: ".SITE_URL.$path.$tr_child->slug);
			exit;
		}
	}
}
else
{
	$tr_page->id = '';
}

if($tr_page->id =='')
{
	die("Page not found");
}


include(SRV_ROOT."includes/meta.php");
?>
</head>

<body class="404">

	<div id="wraper">
	
		<?php include(SRV_ROOT."includes/header.php"); ?>

        <?php include(SRV_ROOT."includes/slideshow.php"); ?>

        <div id="main">
        
        	<div class="container">

	        	<?php include(SRV_ROOT."includes/serviceNav.php"); ?>
	        	
                    <div id="content">
	
						<?php include(SRV_ROOT."includes/content-page.php"); ?>
							
                    </div><!--content-->
                </div> <!--container -->
        </div> <!-- #main -->
        
        
   	    <!--<div class="push"></div>-->
    
	</div><!--wraper--> 

	<?php include(SRV_ROOT."includes/footer.php"); ?>
</body>
</html>