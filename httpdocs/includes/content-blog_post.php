<?php
include_once(SRV_ROOT."admin/pages/class.page.php");
include_once(SRV_ROOT."admin/extensions/projects/functions.php");
include_once(SRV_ROOT."admin/extensions/blogs/functions.php");
include_once(SRV_ROOT."includes/functions.php");

$view = 'page';
$found = false;
$year = isset($_GET['y']) ? $_GET['y'] : '';
$month = isset($_GET['m']) ? $_GET['m'] : '';
$name = isset($_GET['name']) ? $_GET['name'] : '';
$active_year = $year;
$active_month = $month;

if(!($year =='' && $month =='' && $name == ''))
{
	if (!preg_match("/^[0-9]{1,}$/", $year)) die("Invalid year");
	if (!preg_match("/^[0-9]{1,}$/", $month)) die("Invalid month");
	if (!preg_match("/^[a-zA-Z0-9_-]{1,}$/", $name)) die("Invalid article name");
	$post = new blog_post('',$name,SITE_LANG,$year,$month);
	if($post->id != '')
	{
		$found = true;
		$blog = new blog($post->blogID);
		$tr_page = new page('','blog');
		$tr_page->title = $post->title;
		$tr_page->slug = $post->slug;
		$seo_refID = $post->id;
		$seo_keyword = "blog_post";
		$images = $post->get_images();
		$author = new blog_author($post->author);
		$categories = $post->get_categories();		
	}
}

if($found == false)
{
	header("HTTP/1.0 404 Not Found");
	include_once(SRV_ROOT."error_pages/404.php");
	exit;
}

//Replace common terms
$post->content = str_ireplace($find,$replace,$post->content);


include(SRV_ROOT."includes/meta.php");
?>

</head>

<body class="<?=$tl_page->slug?> <?=$tr_page->slug?>">

	<div id="wraper">
	
		<?php include(SRV_ROOT."includes/header.php"); ?>

        <?php include(SRV_ROOT."includes/slideshow.php"); ?>

        <div id="main">
        
        	<div class="container">

	        	<?php include(SRV_ROOT."includes/serviceNav.php"); ?>
	        	
                    <div id="content">


	
						<div class="blog-post">
							<?php displayMessage();?>


							<h1 class="entry-title"><?= $post->title; ?></h1>
							<p class="post-date"><?= $post->get_the_date('F j, Y');?></p>
												
							<div class="entry-content">
								<?php
								$post_images = $post->get_images();
								if(count($post_images)>0)
								{
									for($i=0;$i<1;$i++)
									{
										if(isset($post_images[$i]))
										{
											$img = new image($post_images[$i],'','','image',SITE_LANG);
											?>
											<div class="blog-image">
												<img src="<?=media($img->path)?>" alt="<?=$img->alt?>"/>
											</div>
								
											<?php
											break;
										}
									}
								}
								?>
										
								<div class="post_content">
									<?= $post->content?>
								</div>
		
								<div class="post_sharing">
								
									<span class='st_facebook_hcount' displayText='Facebook'></span>
									<span class='st_twitter_hcount' displayText='Tweet'></span>
									<span class='st_googleplus_hcount' displayText='Google +'></span>
								
								</div>
			
								<div class="post_meta">
									<?php
									if(count($categories)>0)
									{
										?>
										<p>Posted in <?php
										$cats = array();
										foreach($categories as $cat_id)
										{
											$cat = new post_category($cat_id);
											$cats[] = '<a href="'.SITE_URL.'blog/categories/'.$cat->slug.'" title="View all posts listed under \''.$cat->name.'\'">'.$cat->name.'</a>';
										}
										echo implode(", ", $cats);
										?></p>
										<?php
									}
									?>
								</div> <!-- .post_meta -->							
							
							
							</div><!-- .entry-content -->

						</div> <!-- .blog-post -->
				
				
						<?php include(SRV_ROOT."includes/sidebar-blog.php");?>
				
                    </div><!--content-->
                </div> <!--container -->
        </div> <!-- #main -->
        
        
   	    <!--<div class="push"></div>-->
    
	</div><!--wraper--> 

	<?php include(SRV_ROOT."includes/footer.php"); ?>
</body>
</html>