<div class="sidebar">
<div class="blog-category">
			
	<!--!CATEGORIES-->
	<?php
	$categories = $blog->get_categories();
	if(count($categories)>0)
	{
		?>
		<div class="sb_item">
			<h3>Categories</h3>
			<ul>
				<?php
				foreach($categories as $cat_id)
				{
					$cat = new post_category($cat_id);
					?>
					<li<?=$cat_id == $active_cat ? ' class="active"' : ''?>><a href="<?=SITE_URL?>blog/categories/<?=$cat->slug?>"><?=$cat->name?></a></li>
					<?php	
				}
				?>
			</ul>
		</div>
		<?php
	}
	?>

	<!--!ARCHIVES-->
	<?php
	$archives = $blog->get_archive();
	if(count($archives)>0)
	{
		?>
		<div class="sb_item">
			<h3>Archives</h3>
			<?php
			$args = array(	'month_display'=>'name',
							'path2post'=>SITE_URL.'blog/',
							'path2archives'=>SITE_URL.'blog/archives/',
							'lang'=>SITE_LANG,
							'active_year'=>$active_year,
							'active_month'=>$active_month);
			echo $blog->get_archive_html($args);
			?>
		</div>
		<?php
	}
	?>

</div>
</div>