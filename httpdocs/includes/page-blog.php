<?php
// Posts per page
$ppp = 5;

$active_year = '';
$active_month = '';
$active_cat = '';

//!Pagination
$pagination = new Zebra_Pagination();

// set the method to the SEO friendly way
$pagination->method('url');

// Set the base url
$pagination->base_url(SITE_URL.$path,false);

// posts per page
$pagination->records_per_page($ppp);

// change the default labels
$pagination->labels('Newer posts', 'Older posts');
?>

<div class="blog-post">

	<?php
	displayMessage();
	
	$blog = new blog($tr_page->type_val,'',SITE_LANG);

	//!Filter by category
	if(isset($_GET['cat']) && preg_match("/^[a-zA-Z0-9_-]{1,}$/", $_GET['cat']))
	{
		$cat = new post_category('',$_GET['cat']);
		$posts = $blog->get_posts('cat='.$cat->id);
		$pagination->base_url(SITE_URL.$path.'categories/'.$cat->slug.'/',false);
	}
	//!Filter by date (archives)
	elseif(
		(isset($_GET['year']) && preg_match("/^[0-9]{1,}$/", $_GET['year'])) &&
		(isset($_GET['month']) && preg_match("/^[0-9]{1,}$/", $_GET['month']))
		  )
	{
		$y = $_GET['year'];
		$m = $_GET['month'];
		$posts = $blog->get_posts('year='.$y.'&month='.$m);
		$pagination->base_url(SITE_URL.$path."archives/$y/$m/",false);
	}
	//!No filter - Latest posts
	else
	{
		$posts = $blog->get_posts('limit=15');
	}

	//!Adjust page title depending on filter
	if($tr_page->showTitle == 1)
	{
		if(isset($cat) && $cat->id !='') 
		{
			$tr_page->title .= " - ".$cat->name;
		}
		if(isset($_GET['year']) && isset($_GET['month'])) 
		{
			$tr_page->title .= " - ".lang(strtoupper(date("F", mktime(0, 0, 0, $_GET['month'], 10))))." ".$_GET['year'];
		}
	}

	//!Pagination continued
	
	// Total posts
	$pagination->records(count($posts));
	
	// Display only the posts for the current page
	$posts = array_slice(
	    $posts,
	    (($pagination->get_page() - 1) * $ppp),
	    $ppp
	);

	if($tr_page->showTitle == 1)
	{
		$page_title = $tr_page->get_page_title();
		?>
		<h1><?=$page_title['content']?></h1>
		<?php
	}
	$main_text = $tr_page->get_text_block('primary');
	echo $main_text;	
	?>
	<div class="posts">
		<?php
		if(count($posts)==0)
		{
			?>
			<h2>Nothing found</h2>
			<p>No posts have been added yet. Please try again later.</p>
			<?php
		}
		else
		{
			foreach($posts as $post_id)
			{
				$post = new blog_post($post_id,'',SITE_LANG);
				$author = new blog_author($post->author);
				$categories = $post->get_categories();
				$m = $post->get_the_date('m');
				$y = $post->get_the_date('Y');						
				$post_args = array(	'readmore_format'=>'[Read full post]',
									'path2post'=>SITE_URL.$tr_page->slug."/".$y."/".$m."/".$post->slug,
									'char_limit'=>350 );
				?>
				<div class="post">
				
					<h2 class="entry-title"><a href="<?=$post_args['path2post']?>"><?= $post->title; ?></a></h2>
					<p class="post-date"><?= $post->get_the_date('F j, Y');?></p>
					
					<div class="entry-content">
						<?php
						$post_images = $post->get_images();
						if(count($post_images)>0)
						{
							for($i=0;$i<1;$i++)
							{
								if(isset($post_images[$i]))
								{
									$img = new image($post_images[$i],'','','image',SITE_LANG);
									?>
									<div class="blog-image">
										<img src="<?=media($img->path)?>" alt="<?=$img->alt?>"/>
									</div>
									<?php
									break;
								}
							}
						}
						?>
						<div class="post_excerpt">
							<?= $post->get_summary($post_args)?>
						</div>

						<div class="post_sharing">
						
							<span class='st_facebook_hcount' st_url='<?=$post_args['path2post']?>' st_title='<?= $post->title; ?>' displayText='Facebook'></span>
							<span class='st_twitter_hcount' st_url='<?=$post_args['path2post']?>' st_title='<?= $post->title; ?>' displayText='Tweet'></span>
							<span class='st_googleplus_hcount' st_url='<?=$post_args['path2post']?>' st_title='<?= $post->title; ?>' displayText='Google +'></span>
						
						</div>

						<div class="post_meta">
							<?php
							if(count($categories)>0)
							{
								?>
								<p>Posted in <?php
								$cats = array();
								foreach($categories as $cat_id)
								{
									$cat = new post_category($cat_id);
									$cats[] = '<a href="'.SITE_URL.$tr_page->slug.'/categories/'.$cat->slug.'" title="View all posts listed under \''.$cat->name.'\'">'.$cat->name.'</a>';
								}
								echo implode(", ", $cats);
								?></p>
								<?php
							}
							?>
						</div> <!-- .post_meta -->
						
						
					</div><!-- .entry-content -->

				</div> <!-- .post -->
				<?php
			}
		}
		?>
		<div class="pagination"><?php $pagination->render(); ?></div>
	</div> <!-- .posts -->
					
</div>

<?php include(SRV_ROOT."includes/sidebar-blog.php");?>
