<?php
function tinymceInit($preview = SITE_URL,$selector=true)
{
	$output = $selector==true ? "$('textarea.tinymce').tinymce({" : "";
	$output .= "script_url : '".SITE_URL."admin/js/tinymce/tinymce.min.js',
				theme : 'modern',
				skin: 'traffic',
				document_base_url : '".SITE_URL."',
				relative_urls : true,
				convert_urls : true,
				entity_encoding : 'named',
				width: '100%',
				height: 300,
				plugins : 	'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor moxiemanager',
			
				toolbar1: 'bold italic underline | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link insertfile image media | preview fullscreen', 
			   style_formats: [
			        {title: 'Paragraph text', block: 'p'},
			        {title: 'Subheading (H2)', block: 'h2'},
			        {title: 'Sub-subheading (H3)', block: 'h3'},
			        {title: 'Small text', selector: 'h2,h3,p,ul,ol', classes: 'small'}
			    ],

				//invalid_elements : 'div',
				extended_valid_elements: 'img[!src|border:0|alt|title|width|height|style]a[name|href|target|title|onclick]',
				paste_word_valid_elements: '@[class],p,h2,h3,h4,h5,h6,a[href|target],strong/b,div[align],br,table,tbody,thead,tr,td,ul,ol,li,img[src]',
				link_list : '".SITE_URL."admin/pages/pagelist.php',
				target_list: [
			        {title: 'Same page', value: '_self'},
			        {title: 'New page', value: '_blank'}
			    ],
				content_css : 'css/tinymce.css'";
	$output .= $selector==true ? "});" : "";
	return $output;
}

$tinymceInit = tinymceInit(SITE_URL."preview.php");




function getMenuTrail($node)
{
	$query = "SELECT area_id, area_parent FROM `cms_areas` WHERE area_id = '$node'";
	$result = dbQuery($query);
	$row = dbAssoc($result);
	$parent = $row['area_parent'];
	$id = $row['area_id'];
	$trail = array();
	if($parent !='')
	{
		$trail[] = "$parent-$id";
		$trail = array_merge(getMenuTrail($parent), $trail);
	}
	return $trail;
}
function getMenuPath($trail)
{
	$level = 1;
	$path = '';
	foreach($trail as $step)
	{
		$split = explode("-",$step);
		$parent = $split[0];
		$child = $split[1];
		
		if ($step != '0-1')
		{
			$result = dbQuery("SELECT area_dir
								FROM `cms_areas`
								WHERE area_id = '$child'
								ORDER BY area_pos, area_id");
			if (dbRows($result) > 0)
			{
				$row = dbAssoc($result);
				$dir = $row['area_dir'];
				$path .= $dir."/";
			}
			$level++;
		}
	}
	return $path;
}
function getLanding($userID)
{
	$query = "SELECT user_landing FROM `cms_users` WHERE user_id = '$userID'";
	$result = dbQuery($query);
	$row = dbAssoc($result);
	$trail = getMenuTrail($row['user_landing']);
	$path = getMenuPath($trail);
	return $path;
}
?>